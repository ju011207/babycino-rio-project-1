class BugTest2 {
    public static void main(String[] a) {
	    System.out.println(new Test2().two());
    }
}

class Test2 {
    public int two() {
        int x;
        x = 5;
        x = 1; // Bug I2, Instead of incrementing x, set it to 1
        return x;
    }
}