class BugTest1 {
    public static void main(String[] a) {
	    System.out.println(new Test1().one());
    }
}

class Test1 {
    public int one() {
        boolean x;
        x = true;
        x++;  // Bug I1, set x as a boolean value and incremented x
        return x;
    }
}
