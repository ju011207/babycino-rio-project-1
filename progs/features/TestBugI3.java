class BugTest3 {
    public static void main(String[] a) {
	    System.out.println(new Test3().three());
    }
}

class Test3 {
    public int three() {
        int x;
        x = 1;
        return x; //Bug I3, set x to 1 and not modifying x at all. 
    }
}
